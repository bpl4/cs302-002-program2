void menu();
void backend(Adventour<Tour> & mytour, Adventour<Game> & mygame, Adventour<Food> & myfood);
int create_list(list<string> & src);
int create_menu(vector<string> & foods, vector<string> drinks);
void tour_activity(Adventour<Tour> & mytour);
void game_activity(Adventour<Game> & mygame);
void food_activity(Adventour<Food> & myfood);

