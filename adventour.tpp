/* Brian Le

  This is the .tpp file that includes the implementations of the Node and Adventour class*/

// Node
template <class TYPE>
Node<TYPE>::Node(): next(nullptr), previous(nullptr)
{}

template <class TYPE>
Node<TYPE>::~Node()
{
	next.reset();
	previous.reset();
}

template <class TYPE>
Node<TYPE>::Node(const Node<TYPE> & src): next(nullptr), previous(nullptr)
{
	activity = src.activity;
}

template <class TYPE>
Node<TYPE>::Node(const TYPE & src): next(nullptr), previous(nullptr)
{
	activity = src;
}

template <class TYPE>
Node<TYPE> & Node<TYPE>::operator=(const Node<TYPE> & src)
{
	activity = src.activity;
	next = src.next;
	previous = src.previous;
}

template <class TYPE>
bool Node<TYPE>::operator<(const Node<TYPE> & op2) const
{
	return activity < op2.activity;
}

template <class TYPE>
bool Node<TYPE>::operator<=(const Node<TYPE> & op2) const
{
	return activity <= op2.activity;
}

template <class TYPE>
bool Node<TYPE>::operator>(const Node<TYPE> & op2) const
{
	return activity > op2.activity;
}

template <class TYPE>
bool Node<TYPE>::operator>=(const Node<TYPE> & op2) const
{
	return activity >= op2.activity;
}

template <class TYPE>
bool Node<TYPE>::operator==(const Node<TYPE> & op2) const
{
	return activity == op2.activity;
}

template <class TYPE>
bool Node<TYPE>::operator!=(const Node<TYPE> & op2) const
{
	return activity != op2.activity;
}

template <class TYPE>
ostream & operator<<(ostream & out, const Node<TYPE> & op2)
{
	out << op2.activity;
	return out;
}

template <class TYPE>
istream & operator>>(istream & in, Node<TYPE> & op2)
{
	in >> op2.activity;
	return in;
}

template <class TYPE>
typename Node<TYPE>::node_ptr_type & Node<TYPE>::get_next()
{
	return next;
}

template <class TYPE>
typename Node<TYPE>::node_ptr_type & Node<TYPE>::get_previous()
{
	return previous;
}

template <class TYPE>
void Node<TYPE>::set_next(node_ptr_type & new_next)
{
	next = new_next;
}

template <class TYPE>
void Node<TYPE>::set_previous(node_ptr_type & new_prev)
{
	previous = new_prev;
}

template <class TYPE>
void Node<TYPE>::display()
{
	activity.display();
}

template <class TYPE>
bool Node<TYPE>::disarm()
{
	return activity.disarm();
}

template <class TYPE>
bool Node<TYPE>::walkover()
{
	return activity.walkover();
}

template <class TYPE>
int Node<TYPE>::display_menu() const
{
	return activity.display_menu();
}

// Adventour
template <class TYPE>
Adventour<TYPE>::Adventour(): head(nullptr), tail(nullptr), count(0)
{}

template <class TYPE>
Adventour<TYPE>::~Adventour()
{
	remove_all(head);
}

template <class TYPE>
int Adventour<TYPE>::remove_all(node_ptr & head)
{
	int i = 0;

	if (!head)
		return 0;

	i = 1 + remove_all(head->get_next());

	head.reset();
	return i;
}

template <class TYPE>
Adventour<TYPE>::Adventour(const Adventour<TYPE> & src)
{	
	Empty_List empty;
	if (!src.head)
		throw (empty);
	
	copy(head, src.head);
	count = src.count;
}

template <class TYPE>
int Adventour<TYPE>::copy(node_ptr & desthead, const node_ptr & srchead)
{
	int i = 0;

	if (!srchead) // at the end
		return 0;

	//desthead = srchead; // smart points solves the double free.
	
	shared_ptr<Node<TYPE>> temp(new Node<TYPE>(*srchead));
	desthead = temp;

	i = 1 + copy(head->get_next(), srchead->get_next());

	// connect the previous nodes on the unwind

	if (head->get_next())
	{
		head->get_next()->set_previous(head);
	}

	return i;
}


template <class TYPE>
Adventour<TYPE> & Adventour<TYPE>::operator=(const Adventour<TYPE> & src)
{
	copy(head, src.head);	
	count = src.count;
}

template <class TYPE>
bool Adventour<TYPE>::operator<(const Adventour<TYPE> & op2) const
{
	return count < op2.count;	
}

template <class TYPE>
bool Adventour<TYPE>::operator<=(const Adventour<TYPE> & op2) const
{
	return count <= op2.count;
}

template <class TYPE>
bool Adventour<TYPE>::operator>(const Adventour<TYPE> & op2) const
{
	return count > op2.count;
}

template <class TYPE>
bool Adventour<TYPE>::operator>=(const Adventour<TYPE> & op2) const
{
	return count >= op2.count;
}

template <class TYPE>
bool Adventour<TYPE>::operator==(const Adventour<TYPE> & op2) const
{
	return count == op2.count;
}

template <class TYPE>
bool Adventour<TYPE>::operator!=(const Adventour<TYPE> & op2) const
{
	return count != op2.count;
}

template <class TYPE>
Adventour<TYPE> & Adventour<TYPE>::operator+=(const int steps) // this will traverse the list. moving forward x amount of nodes.
{
	Empty_List empty;

	try
	{
		if (!head)
			throw (empty);
	}

	catch(Empty_List)
	{
		//cerr << "There is nothing on the database!" << endl;
		return *this;
	}

	traverse_forwards(current, steps);

	return *this;
}

template <class TYPE>
Adventour<TYPE> & Adventour<TYPE>::operator-=(const int steps)
{
	Empty_List empty;

	try 
	{
		if (!head)
			throw (empty);
	}

	catch (Empty_List)
	{
		//cerr << "There is nothing on the database!" << endl;
		return *this;
	}

	traverse_backwards(current, steps);
	return *this;
}

template <class TYPE>
int Adventour<TYPE>::traverse_forwards(node_ptr & current, const int steps)
{
	int i = 0;
	
	if (!current->get_next()) // at the last node
	{
		//cout << "We are at the last page" << endl;
		return i;
	}

	if (!current) // not supposed to be here...
	{
		current = head;
		return i;
	}

	++i;
	current = current->get_next();

	if (i == steps)
		return i;
	
	return traverse_forwards(current, steps);
}

template <class TYPE>
int Adventour<TYPE>::traverse_backwards(node_ptr & current, const int steps)
{
	int i = 0;
	
	if (!current->get_previous()) // at the last node
	{
		//cout << "We are at the last page" << endl;
		return i;
	}

	if (!current) // not supposed to be here...
	{
		current = head;
		return i;
	}

	++i;
	current = current->get_previous();

	if (i == steps)
		return i;
	
	return traverse_forwards(current, steps);
}


template <class TYPE>
bool Adventour<TYPE>::insert(const TYPE & activity)
{
	if (!head) // if the list is empty
	{
		shared_ptr<Node<TYPE>> temp(new Node<TYPE>(activity));
		head = move(temp);
		tail = head;
		current = head;
		++count;

		return true;
	}

	shared_ptr<Node<TYPE>> temp(new Node<TYPE>(activity));
	temp->set_previous(tail);
	tail->set_next(temp);
	tail = tail->get_next();
	++count;
	return true;

}

template <class TYPE>
ostream & operator<<(ostream & out, const Adventour<TYPE> & op2)
{
	op2.display_activity();	
	return out;
}

template <class TYPE>
int Adventour<TYPE>::display() const
{
	Empty_List empty;

	if (!head)
		throw(empty);
	
	return display(head);
}

template <class TYPE>
int Adventour<TYPE>::display(const node_ptr & head) const
{
	int i = 0;

	if (!head)
		return 0;
	
	cout << *head << endl;

	i = 1 + display(head->get_next());
	return i;
}

template <class TYPE>
int Adventour<TYPE>::numofdata() const
{
	return count;
}

template <class TYPE>
bool Adventour<TYPE>::display_activity() const
{
	if (!head)
	{
		cout << "There is currently nothing on the database!" << endl;
		return false;
	}

	if (current)
		current->display();

	return true;
}

template <class TYPE>
bool Adventour<TYPE>::defuse()
{
	if (current)
		return current->disarm();
	return false;
}

template <class TYPE>
bool Adventour<TYPE>::walkover()
{
	if (current)
		return current->walkover();
	return false;
}

template <class TYPE>
void Adventour<TYPE>::restart()
{
	if (head && current)
		current = head;
	
	return;
}

template <class TYPE>
int Adventour<TYPE>::display_menu() const
{
	if (current)
		return current->display_menu();
	
	return 0;
}
