#include "class.h"
#include "prog_func.h"

/*
void menu();
void backend(Adventour<Tour> & mytour, Adventour<Game> & mygame, Adventour<Food> & myfood);
int create_list(list<string> & src);
*/

void menu()
{	
	cout << "Welcome to Adventour!\n" << endl;

	cout << "Please select an option:\n";
	cout << "1. Visit a Tourist Location\n"\
		"2. Play a sidescroller Game\n"\
		"3. View a Restaraunt and their Menus\n"\
		"4. Quit\n";

}

void backend(Adventour<Tour> & mytour, Adventour<Game> & mygame, Adventour<Food> & myfood)
{
	bool program = true;
	int admin_option = 0;

	// Activity's data
	char act_name[100];

	char rain;
	bool whileRaining = false;

	// Tour's data
	char location[100];
	float ticket_cost = 0.00;
	list<string> merch;

	// Game's data
	int level = 0;
	/*
	bool is_trap = false;
	bool is_prize = false;
	bool game_over = false;
	int score = 0;
	*/
	char game_create;

	// Restaraunt's data
	string restaurant;
	vector<string> foods;
	vector<string> drinks;




	while (program)
	{
		cout << "BACKEND MENU\n";


		cout << "Please select and option:\n";
		cout << "1. Add a Tourist Location\n"\
			"2. Create a Sidescroller Game\n"\
			"3. Add a Restaraunt\n"\
			"4. Quit\n";
		
		cin >> admin_option;
		cin.ignore(100, '\n');


		switch(admin_option)
		{
			case 1:
				{
					cout << "Enter the name of the Tourist place: ";
					cin.get(act_name, 100, '\n');
					cin.ignore(100, '\n');

					cout << "Can this activity be done while raining? (Y/N): ";
					cin >> rain;
					cin.ignore(100, '\n');

					if (rain == 'y' || 'Y')
						whileRaining = true;
					else
						whileRaining = false;

					cout << "Enter the location of " << act_name << ": ";
					cin.get(location, 100, '\n');
					cin.ignore(100, '\n');

					cout << "Enter the ticket cost: $";
					cin >> ticket_cost;
					cin.ignore(100, '\n');

					create_list(merch);

					Tour toadd(location, ticket_cost, merch, act_name, whileRaining);
					mytour.insert(toadd);
					break;
				}
			case 2:
				{
					cout << "Create a Sidescroller Game? (Y/N): ";
					cin >> game_create;
					cin.ignore(100, '\n');

					if (game_create == 'y' || game_create == 'Y')
					{
						cout << "Enter the name of the game: ";
						cin.get(act_name, 100, '\n');
						cin.ignore(100, '\n');
						cout << "Can this activity be done while raining? (Y/N): ";
						cin >> rain;
						cin.ignore(100, '\n');

						cout << "How many levels should be generated? Enter the number of levels: ";
						cin >> level;
						cin.ignore(100, '\n');

						for (int i = 0; i < level; ++i) // creating the levels.
						{
							Game new_level(i + 1, act_name, rain);
							mygame.insert(new_level);
						}
					}

					break;
				}
			case 3:
				{
					cout << "Enter the name of the Restaurant: ";
					cin.get(act_name, 100, '\n');
					cin.ignore(100, '\n');

					cout << "Can you eat here while raining? (Y/N): ";
					cin >> rain;
					cin.ignore(100, '\n');

					if (rain == 'y' || 'Y')
						whileRaining = true;
					else
						whileRaining = false;

					cout << "What kind of restaurant is this?: ";
					getline(cin, restaurant);

					create_menu(foods, drinks);

					Food new_restaurant(restaurant, foods, drinks, act_name, rain);
					myfood.insert(new_restaurant);

					break;
				}
			case 4:
				program = false;
				break;
			default:
				//
				break;

		}
	}
}

int create_list(list<string> & src)
{
	string items;

	cout << "Enter the name of items and merchandise that the tourist place is selling. Type -1 to finish\n";

	do
	{
		getline(cin, items);
		if (items != "-1")
			src.push_back(items);

	} while(items != "-1");

	return src.size();
}

int create_menu(vector<string> & foods, vector<string> drinks)
{
	string food_item;
	string drink_item;

	cout << "Enter the names of the foods that this restaurant is catering. Type -1 to finish\n";

	do
	{
		getline(cin, food_item);
		if (food_item != "-1")
			foods.push_back(food_item);

	} while(food_item != "-1");

	cout << "Enter the names of the drinks that this restaurant is catering. Type -1 to finish\n";

	do
	{
		getline(cin, drink_item);
		if (drink_item != "-1")
			drinks.push_back(drink_item);

	} while(drink_item != "-1");

	return foods.size() + drinks.size();


}

void tour_activity(Adventour<Tour> & mytour)
{
	bool program = true;
	int option = 0;


	while (program)
	{
		cout << endl;
		mytour.display_activity();
		cout << "\n1. Next Page"\
			"\n2. Previous Page\n" 
			"3. Exit" << endl;
		cin >> option;
		cin.ignore(100, '\n');

		switch (option)
		{
			case 1:
				mytour += 1;
				break;
			case 2:
				mytour -= 1;
				break;
			case 3:
				program = false;
				break;
			default:
				cout << "Not an option. Try again." << endl;
				break;
		}
	}
}


void game_activity(Adventour<Game> & mygame)
{
	bool program = true;
	int option = 0;

	while (program)
	{
		cout << endl;
		mygame.display_activity();

		cout << "\n1. Defuse Bomb\n"\
			"2. Move to next level\n"
			"3. Exit" << endl;
		cin >> option;
		cin.ignore(100, '\n');

		switch (option)
		{
			case 1:
				if (mygame.defuse())
					mygame += 1;
				else
					mygame.restart();
				break;
			case 2:
				if (mygame.walkover())
					mygame += 1;
				else
					mygame.restart();

				break;
			case 3:
				program = false;
				break;
			default:
				break;
		}
	}
}

void food_activity(Adventour<Food> & myfood)
{
	bool program = true;
	int option = 0;

	while (program)
	{
		cout << endl;
		myfood.display_activity();

		cout << "\n1. Next Page"\
			"\n2. Previous Page\n"
			"3. Exit" << endl;
		cin >> option;
		cin.ignore(100, '\n');

		switch (option)
		{
			case 1:
				myfood += 1;
				break;
			case 2:
				myfood -= 1;
				break;
			case 3:
				program = false;
				break;
			default:
				break;
		}
	}

}
