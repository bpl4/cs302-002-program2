#include <iostream>
#include <iomanip>
#include <memory>
#include <cstring>
#include <string>
#include <cstdlib>
#include <list>
#include <array>
#include <vector>
using namespace std;

/* Brian Le

   This is the file that contains all the classes and their prototypes.
   */

class Activity // base class
{
	public:
		Activity();
		~Activity();
		Activity(const Activity & src);
		Activity(char * activity_name, bool rain);

		Activity & operator=(const Activity & src);
		bool operator<(const Activity & op2) const;
		bool operator<=(const Activity & op2) const;
		bool operator>(const Activity & op2) const;
		bool operator>=(const Activity & op2) const;
		bool operator==(const Activity & op2) const;
		bool operator!=(const Activity & op2) const;
		friend ostream & operator<<(ostream & out, const Activity & op2); 
		friend istream & operator>>(istream & in, Activity & op2);


		//void display() const;
		bool copy(char * source, bool rain); // deep copy
		bool doWhileRaining() const; // returns if the activity can be done when raining.
		void display() const;

	protected:
		char * name;
		bool whileRaining; // can it be done while raining?
};

class Tour: public Activity // Oaks Park, Portland Japanese Garden, etc.
{
	public:
		Tour();
		~Tour();
		Tour(char *, float, list<string>, char * activity, bool rain);
		Tour(const Tour & src);
		Tour & operator=(const Tour & src);
		bool operator<(const Tour & op2) const;
		bool operator<=(const Tour & op2) const;
		bool operator>(const Tour & op2) const;
		bool operator>=(const Tour & op2) const;
		bool operator==(const Tour & op2) const;
		bool operator!=(const Tour & op2) const;
		friend ostream & operator<<(ostream & out, const Tour & op2);
		friend istream & operator>>(istream & in, Tour & op2);


		bool copy(char *, float, list<string>);
		void display();
		int display_merch();
	
	private:
		char * location;
		float ticket_cost; // cost to get into these tourist places.
		list<string> merch; // for people to see what the place is selling.

};

class Game: public Activity // This is a "sidescroller". Each Game object is a "level" or "section"
{
	public:
		Game();
		Game(int, bool, bool, bool, int, char * activity, bool rain);// manually set the level
		Game(int level_set, char * activity, bool rain); // randomly generate x levels.
		~Game();

		//Game & operator=(const Game & src); // likely not needed
		bool operator<(const Game & op2) const;
		bool operator<=(const Game & op2) const;
		bool operator>(const Game & op2) const;
		bool operator>=(const Game & op2) const;
		bool operator==(const Game & op2) const;
		bool operator!=(const Game & op2) const;
		friend ostream & operator<<(ostream & out, const Game & op2);
		friend istream & operator>>(istream & in, Game & op2);

		
		//void display() const; // display the level and whats in it. bombs? prizes?
		bool disarm(); // lets player attempt to disarm trap (rng). must roll a 15 or higher to disarm. 
		bool walkover();
		int diceroll(); // DnD style D20 dice simulator.
		bool random_generate(int level_set); // randomly generate the level
		bool display();
		bool alive() const;

		

	private:
		//string title; // title of the game but I think this is not needed...
		int level;
		bool is_trap;
		bool is_prize;
		bool game_over;
		int score; // +10 for every prize recieved, +5 for every bomb defused.
};


class Food: public Activity
{
	public:
		Food();
		Food(string &, vector<string> &, vector<string> &, char * activity, bool whileRaining);
		~Food();
		//Food(const Food & src); // copy constructor likely not needed
		//void display() const;

		//Food & operator=(const Food & src);
		bool operator<(const Food & op2) const;
		bool operator<=(const Food & op2) const;
		bool operator>(const Food & op2) const;
		bool operator>=(const Food & op2) const;
		bool operator==(const Food & op2) const;
		bool operator!=(const Food & op2) const;
		friend ostream & operator<<(ostream & out, const Food & op2);
		friend istream & operator>>(istream & in, Food & op2);

		int display_menu() const;
		bool display() const;

	private:
		string restaurant; // type of restaurant: pancake house? steakhouse? etc.
		vector<string> food;
		vector<string> drink;
};


template <class TYPE>
class Node			// Node does not have access to the subclass's private or protected stuff!
{
	public:
		Node<TYPE>();
		~Node<TYPE>();
		Node<TYPE>(const Node<TYPE> & src);
		Node<TYPE>(const TYPE & src);

		Node<TYPE> & operator=(const Node<TYPE> & src);
		bool operator<(const Node<TYPE> & op2) const;
		bool operator<=(const Node<TYPE> & op2) const;
		bool operator>(const Node<TYPE> & op2) const;
		bool operator>=(const Node<TYPE> & op2) const;
		bool operator==(const Node<TYPE> & op2) const;
		bool operator!=(const Node<TYPE> & op2) const;

		template <class T>
		friend ostream & operator<<(ostream & out, const Node<T> & op2);
		
		template <class T>
		friend istream & operator>>(istream & in, Node<T> & op2);

		/*
		Node<TYPE> & operator+=(const Node<TYPE> & op2); 
		Node<TYPE> & operator-=(const Node<TYPE> & op2); 
		*/


		typedef shared_ptr<Node<TYPE>> node_ptr_type; // using smart pointers

		//Note to self: reset() assigns it to nullptr or a new object. 
		//since we are working with DLL, we have to use shared_ptr

		node_ptr_type & get_next();
		node_ptr_type & get_previous();
		void set_next(node_ptr_type & new_next);
		void set_previous(node_ptr_type & new_prev);

		void display();

		// for the game class
		bool disarm();
		bool walkover();

		// food class
		int display_menu() const;

	private:
		TYPE activity;
		node_ptr_type next;
		node_ptr_type previous;

};

// Containing Class
template <class TYPE>
class Adventour // Adventour will be the name of my software (Adventure + Tour = Adventour)
{
	public:
		Adventour<TYPE>();
		~Adventour<TYPE>();
		Adventour<TYPE>(const Adventour<TYPE> & src);

		// Overloaded Operators
		Adventour<TYPE> & operator=(const Adventour<TYPE> & src);
		bool operator<(const Adventour<TYPE> & op2) const;
		bool operator<=(const Adventour<TYPE> & op2) const;
		bool operator>(const Adventour<TYPE> & op2) const;
		bool operator>=(const Adventour<TYPE> & op2) const;
		bool operator==(const Adventour<TYPE> & op2) const;
		bool operator!=(const Adventour<TYPE> & op2) const;

		template <class T>
		friend ostream & operator<<(ostream & out, const Adventour<T> & op2);
		
		template <class T>
		friend istream & operator>>(istream & in, Adventour<T> & op2);

		Adventour<TYPE> & operator+=(const int steps); // this will traverse the list. moving forward x amount of nodes.
		Adventour<TYPE> & operator-=(const int steps); // going backwards

		typedef shared_ptr<Node<TYPE>> node_ptr; // using smart pointers to manage data structures.
		
		// Functions

		bool insert(const TYPE & activity); // inserting stuff into the DLL
		/*
		bool next_tour();
		bool next_level();
		bool next_food();
		*/
		int display() const; // displays all the objects
		int numofdata() const;
		bool display_activity() const; // called by the cout operator.

		// Game class 
		bool defuse();
		bool walkover();
		void restart(); // restart game if player dies

		// Food class
		int display_menu() const;



	private:
		int remove_all(node_ptr & head);
		int copy(node_ptr & desthead, const node_ptr & srchead);
		bool insert(node_ptr & head, TYPE & activity);
		int display(const node_ptr & head) const;
		int display_activity(const node_ptr & head) const;
		int traverse_forwards(node_ptr & current, const int steps);
		int traverse_backwards(node_ptr & current, const int steps);

		node_ptr current;
		node_ptr head;
		node_ptr tail;
		int count; // num of nodes
};

struct Empty_List
{
	int expected_items;
};

struct Null_Values
{
	int null_warning;
};


#include "adventour.tpp"
