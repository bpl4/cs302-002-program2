#include "class.h"
/* Brian Le

   This file includes the implementations of the Activity, Tour, Game, and Food member functions.
   */

// Activity Base CLass
Activity::Activity(): name(nullptr), whileRaining(false)
{}

Activity::~Activity()
{
	if (name)
		delete [] name;
}

Activity::Activity(const Activity & src): name(nullptr), whileRaining(false)
{
	copy(src.name, src.whileRaining);
}

Activity::Activity(char * activity_name, bool rain): name(nullptr), whileRaining(false)
{
	copy(activity_name, rain);
}

bool Activity::copy(char * source, bool rain) // deep copy
{
	if (!source)
		return false;

	if (name)
		delete [] name;

	name = new char[strlen(source) + 1];
	strcpy(name, source);

	whileRaining = rain;

	return true;
}

Activity & Activity::operator=(const Activity & src)
{
	if (this == &src)
		return *this;

	if (name)
		delete [] name;

	copy(src.name, src.whileRaining);
	return *this;
}

bool Activity::operator<(const Activity & op2) const
{
	if (!name || !op2.name)
		return false;

	if (strcmp(name, op2.name) < 0)
		return true;
	else
		return false;
}

bool Activity::operator<=(const Activity & op2) const
{

	if (!name || !op2.name)
		return false;

	if (strcmp(name, op2.name) <= 0)
		return true;
	else
		return false;

}

bool Activity::operator>(const Activity & op2) const
{
	if (!name || !op2.name)
		return false;

	if (strcmp(name, op2.name) > 0)
		return true;
	else
		return false;
}

bool Activity::operator>=(const Activity & op2) const
{
	if (!name || !op2.name)
		return false;

	if (strcmp(name, op2.name) >= 0)
		return true;
	else
		return false;

}

bool Activity::operator==(const Activity & op2) const
{
	if (!name || !op2.name)
		return false;

	if (strcmp(name, op2.name) == 0)
		return true;
	else
		return false;
}

bool Activity::operator!=(const Activity & op2) const
{
	if (!name || !op2.name)
		return false;

	if (strcmp(name, op2.name) != 0)
		return true;
	else
		return false;
}

ostream & operator<<(ostream & out, const Activity & op2)
{
	Null_Values null_val;

	if (!op2.name)
	{
		throw(null_val);
	}

	if (op2.name)
		out << op2.name;

	if (op2.whileRaining)
		out << '\t' << "Yes";
	else
		out << '\t' << "No";

	return out;
}

istream & operator>>(istream & in, Activity & op2)
{
	if (op2.name)
		delete [] op2.name;

	char temp[100];
	in.get(temp, 100, '\n');
	in.ignore(100, '\n');
	
	op2.name = new char[strlen(temp) + 1];
	strcpy(op2.name, temp);

	/*
	cin >> op2.whileRaining;
	cin.ignore(100, '\n');
	*/

	return in;
}

bool Activity::doWhileRaining() const
{
	if (whileRaining)
	{
		cout << "Can do this activity while raining" << endl;
		return true;
	}
	else
	{
		cout << "Cannot do this activity while raining" << endl;
		return false;
	}
}

void Activity::display() const
{
	if (name)
		cout << "Name: " << name << endl;

	cout << "Can be done while raining?: ";
	if (whileRaining)
		cout << "Yes\n";
	else
		cout << "No\n";
}






// Tour Sub-Class
Tour::Tour(): location(nullptr), ticket_cost(0.00)
{}

Tour::~Tour()
{
	if (location)
		delete [] location;
}

Tour::Tour(char * loc, float price, list<string> items, char * activity, bool rain):\
		Activity(activity, rain), location(nullptr), ticket_cost(price), merch(items)
{
	if (!location)
	{
		location = new char[strlen(loc) + 1];
		strcpy(location, loc);
	}
}

Tour::Tour(const Tour & src): Activity(src), location(nullptr), ticket_cost(0.00)
{
	copy(src.location, src.ticket_cost, src.merch);
}

bool Tour::copy(char * loc, float price, list<string> items)
{
	if (!loc)
		return false;

	if (location)
		delete [] location;

	location = new char[strlen(loc) + 1];
	strcpy(location, loc);

	ticket_cost = price;
	merch = items;

	return true;
}

Tour & Tour::operator=(const Tour & src)
{
	if (this == &src)
		return *this;

	static_cast<Activity&> (*this) = src;

	copy(src.location, src.ticket_cost, src.merch);
	return *this;
}

bool Tour::operator<(const Tour & op2) const
{
	if (strcmp(location, op2.location) < 0)
		return true;
	else
		return false;
}

bool Tour::operator<=(const Tour & op2) const
{
	if (strcmp(location, op2.location) <= 0)
		return true;
	else
		return false;
}

bool Tour::operator>(const Tour & op2) const
{
	if (strcmp(location, op2.location) > 0)
		return true;
	else
		return false;
}

bool Tour::operator>=(const Tour & op2) const
{
	if (strcmp(location, op2.location) >= 0)
		return true;
	else
		return false;

}

bool Tour::operator==(const Tour & op2) const
{
	if (strcmp(location, op2.location) == 0)
		return true;
	else
		return false;
}

bool Tour::operator!=(const Tour & op2) const
{
	if (strcmp(location, op2.location) != 0)
		return true;
	else
		return false;

}

ostream & operator<<(ostream & out, const Tour & op2)
{
	try
	{
		out << static_cast<const Activity&> (op2) << '\t';
	}

	catch (Null_Values)
	{
		cerr << "The Activity is missing a name." << '\t';
	}
	
	if (op2.location)
		cout << op2.location;
	else
		
	cout << '\t' <<fixed << setprecision(2) << op2.ticket_cost;

	return out;
}

istream & operator>>(istream & in, Tour & op2)
{
	in >> static_cast<Activity&> (op2); // kickstart base class

	if (op2.location)
		delete[] op2.location;

	char temp[100];
	in.get(temp, 100, '\n');
	in.ignore(100, '\n');

	op2.name = new char[strlen(temp) + 1];
	strcpy(op2.name, temp);

	/*

	cin >> op2.ticket_cost;
	cin.ignore(100, '\n');

	// Get the user to input stuff for list until a -1 is typed
	string input;
	do
	{
		getline(cin, input);
		if (input != "-1")
			op2.merch.push_back(input);

	} while (input != "-1");
	*/

	return in;
}

void Tour::display()
{
	Activity::display();
	if (location)
		cout << "Location: " << location << endl;

	cout << "Ticket cost: " << ticket_cost << endl;

	display_merch();
}

int Tour::display_merch()
{
	int list_size = merch.size();

	if (list_size == 0)
	{
		cout << "There are currently no merchandise being sold\n";
		return 0;
	}

	cout << "Here are the list of merchandise that you can buy:\n";

	list<string>::iterator it;
	for (it = merch.begin(); it != merch.end(); ++it)
		cout << *it << endl;

	return list_size;
}


// Game Sub Class
Game::Game(): level(1), is_trap(false), is_prize(false), game_over(false), score(0)
{}

Game::Game(int set_level, bool trap, bool prize, bool game, int set_score, char * activity, bool rain): \
		Activity(activity, rain), level(set_level), is_trap(trap), is_prize(prize), game_over(game), score(set_score)
{
	
}

Game::Game(int level_set, char * activity, bool rain): Activity(activity, rain), level(1), is_trap(false), is_prize(false), game_over(false), score(0)
{
	random_generate(level_set);
}

Game::~Game(){}


bool Game::operator<(const Game & op2) const
{
	if (level < op2.level)
		return true;
	else
		return false;
}

bool Game::operator<=(const Game & op2) const
{
	if (level <= op2.level)
		return true;
	else
		return false;

}

bool Game::operator>(const Game & op2) const
{
	if (level > op2.level)
		return true;
	else
		return false;
}

bool Game::operator>=(const Game & op2) const
{
	if (level >= op2.level)
		return true;
	else
		return false;

}

bool Game::operator==(const Game & op2) const
{
	if (level >= op2.level)
		return true;
	else
		return false;
}

bool Game::operator!=(const Game & op2) const
{
	if (level != op2.level)
		return true;
	else
		return false;
}

ostream & operator<<(ostream & out, const Game & op2)
{
	try
	{
		out << static_cast<const Activity&> (op2) << '\t';
	}

	catch (Null_Values)
	{
		cerr << "The Activity is missing a name." << '\t';
	}
	out << op2.level << '\t';

	if (op2.is_trap)
		out << "Yes" << '\t';
	else
		out << "No" << '\t';

	if (op2.is_prize)
		out << "Yes" << '\t';
	else
		out << "No" << '\t';

	out << op2.score;

	return out;
}

istream & operator>>(istream & in, Game & op2)
{
	in >> op2.level;
	in.ignore(100, '\n');

	in >> op2.is_trap;
	in.ignore(100, '\n');

	in >> op2.is_prize;
	in.ignore(100, '\n');

	in >> op2.score;
	in.ignore(100, '\n');

	return in;
}

int Game::diceroll()
{
	int dice = rand() % 20 + 1; // picks a number from 1 to 20.
	return dice;
}

bool Game::random_generate(int level_set)
{
	game_over = false;
	int gen_id = rand() % 4 + 1;

	switch (gen_id)
	{
		case 1: // prize
			is_prize = true;
			break;

		case 2: // bomb
			is_trap = true;
			break;

		case 3: // bomb + prize
			is_prize = true;
			is_trap = true;
			break;
		default:
			break;
	}


	level = level_set;
	return true;
}

bool Game::disarm()
{
	if (!is_trap || game_over)
		return false;

	int disarm_dc = 10; // player must roll a 10 or higher to disarm this bomb.

	int dice = rand() % 20 + 1;

	cout << "DC to defuse this bomb is: " << disarm_dc << endl;

	cout << "You rolled: " << dice << endl;

	if (dice < disarm_dc) // boom!
	{
		game_over = true;
		cout << "The bomb exploded!" << endl;
		cout << "Game Over" << endl;
		cout << "Score: " << score << endl;
		return false;
	}

	cout << "You have successfully defused the bomb! +5 points\n";
	score += 5;

	if (is_prize)
	{
		cout << "You also found a cool prize! +10\n";
		score += 10;
	}

	return true;
}

bool Game::walkover()
{
	if (game_over)
		return false;
	if (!is_trap)
		return true;


	int dc = 15; // player must roll a 10 or higher to disarm this bomb.

	int dice = rand() % 20 + 1;

	if (dice < dc) // boom!
	{
		//game_over = true;
		cout << "As you walk over the bomb... The bomb explodes!" << endl;
		cout << "Game Over" << endl;
		//cout << "Score: " << score << endl;
		return false;
	}

	cout << "As you walk over the bomb... It did not explode! +10 points\n";
	score += 10;

	cout << "DC to walk over this bomb is: " << dc << endl;

	cout << "You rolled: " << dice << endl;

	if (is_prize)
	{
		cout << "You also found a cool prize! +10\n";
		score += 10;
	}

	return true;

}

bool Game::display()
{
	cout << "Level: " << level << endl;
	
	if (is_trap)
	{
		cout << "There is a bomb!" << endl;
	}
	else
		cout << "There are no bombs at this level." << endl;

	if (is_prize)
	{
		cout << "There is a prize!" << endl;
	}
	else
		cout << "There are no prizes at this level." << endl;
	
	/*
	if (is_trap)
	{
		cout << "\nThere is a bomb! Attempt to defuse? (Y/N): ";
		cin >> option;
		cin.ignore(100, '\n');

		if (option == 'y' || option == 'Y')
			disarm();
		else
			walkover();
	}
	*/

	if (is_prize && !is_trap)
		score += 10;

	return true;
}
	
bool Game::alive() const
{
	if (game_over)
		return false;
	else
		return true;
}


Food::Food()
{}

Food::Food(string & name, vector<string> & food_list, vector<string> & drink_list, char * activity, bool whileRaining): Activity(activity, whileRaining),\
														      restaurant(name), food(food_list), drink(drink_list)
{
		
}



Food::~Food()
{}

bool Food::operator<(const Food & op2) const
{
	if (restaurant < op2.restaurant)
		return true;
	else
		return false;
}

bool Food::operator<=(const Food & op2) const
{
	if (restaurant <= op2.restaurant)
		return true;
	else
		return false;
}


bool Food::operator>(const Food & op2) const
{
	if (restaurant > op2.restaurant)
		return true;
	else
		return false;

}

bool Food::operator>=(const Food & op2) const
{
	if (restaurant >= op2.restaurant)
		return true;
	else
		return false;
}

bool Food::operator==(const Food & op2) const
{
	if (restaurant == op2.restaurant)
		return true;
	else
		return false;
}

bool Food::operator!=(const Food & op2) const
{
	if (restaurant != op2.restaurant)
		return true;
	else
		return false;
}

ostream & operator<<(ostream & out, const Food & op2)
{
	try
	{
		out << static_cast<const Food&> (op2) << '\t';
	}

	catch (Null_Values)
	{
		cerr << "The Activity is missing a name." << '\t';
	}

	out << op2.restaurant;
	return out;
}

istream & operator>>(istream & in, Food & op2)
{
	getline(in, op2.restaurant);
	return in;
}

bool Food::display() const
{
	Activity::display();

	cout << "Restaurant Type: " << restaurant << endl;
	return true;
}

int Food::display_menu() const
{
	int foodsize = food.size();
	int drinksize = drink.size();

	if (foodsize == 0)
		cout << "There are no food in the menu\n";
	else
		cout << "Here are the foods in the menu:\n";

	cout << endl;

	for (int i = 0; i < foodsize; ++i)
	{
		cout << food[i] << endl;
	}

	if (drinksize == 0)
		cout << "There are no drinks in the menu\n";
	else
		cout << "Here are the drinks in the menu:\n";

	for (int i = 0; i < drinksize; ++i)
		cout << drink[i] << endl;

	cout << endl;

	return foodsize + drinksize;
}

