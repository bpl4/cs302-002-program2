#include "class.h"
#include "prog_func.h"


int main()
{
	bool program = true;
	int user_option = 0;


/*	
	{  test block
		Adventour<Tour> mytour; 
		Adventour<Game> mygame;
		Adventour<Food> myfood;

		char loc[] = "Portland";
		char act[] = "JP Garden";
		list<string> test;
		test.push_back("Omiyage");
		test.push_back("Pottery");
		Tour place(loc, 3.99, test, act, false);
		Tour place2(loc, 3.20, test, act, false);
		mytour.insert(place);
		mytour.insert(place2);

		char game[] = "Sidescroller";


		Game leveltest(1, false, true, false, 0, game, true);
		mygame.insert(leveltest);

		//Food mcdonalds(

		cout << mytour << endl;
		cout << mygame << endl;


		Adventour<Tour> anothertour(mytour);
		cout << anothertour << endl;

		try
		{
			cout << myfood << endl;
		}

		catch (Empty_List)
		{
			cerr << "Empty List" << endl;
		}


	}*/

	Adventour<Tour> mytour;
	Adventour<Game> mygame;
	Adventour<Food> myfood;

	while (program)
	{
		menu();
		cin >> user_option;
		cin.ignore(100, '\n');
		
		switch(user_option)
		{
			case 1:	
				tour_activity(mytour);
				break;
			case 2:
				game_activity(mygame);
				break;
			case 3:
				food_activity(myfood);
				break;
			case 4:
				program = false;
				break;
			case -1:
				backend(mytour, mygame, myfood);
				break;
			default:
				break;
		}
	}

	return 0;
}
